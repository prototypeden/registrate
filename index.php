<?php
include("include/db_connect.php");
session_start();
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</head>
<body>
<?php
$login = $_SESSION['login'];
if (!empty($_SESSION['auth'])) {
    $q = mysqli_query($link, "SELECT * FROM users where login='$login'");
    while ($row = mysqli_fetch_assoc($q)) {
        echo '
        <div class="media ml-2 mt-2 img-rounded">
        <img class="avatar rounded-circle" src="/avatars/' . $row['image'] . '" width="64" height="64"
             alt="images">
        <div class="media-body">
            <h5 class="mt-3 ml-1">Здравствуйте,  ' . $_SESSION['login'] . ' !</h5>
        <form action="exit.php">
         <input type="submit" name="exit1" id="exit" class="btn-danger rounded" value="Выйти">
        </form>

        </div>
        </div>';
    }
} else {
    ?>
    <form action="registration.php">
        <input type="submit" class="btn btn-primary mt-2 mr-2 ml-0 float-right" value="Зарегистрироваться">
    </form>
    <form action="login.php">
        <input type="submit" class="btn btn-primary mt-2 mr-2 float-right" value="Войти">
    </form>
<?php }
if (isset($_POST['exit1']))
    session_destroy(); ?>
</body>
</html>
