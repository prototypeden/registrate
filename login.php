<?php
include("include/db_connect.php");
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script src="/js/validate.js"></script>
</head>
<body>
<div class="form mx-auto mt-5 p-3" style="width: 350px; box-shadow: 0 0 10px rgba(0,0,0,0.5);">
    <form class="form-horizontal needs-validation" action="check_login.php" method="POST" novalidate>
        <div class="form-group text-center">
            <div class="form-group">
                <label for="validationCustomUsername" class="col-sm-3 control-label">Логин</label>
                <div class="col-sm-10 mx-auto">
                    <input type="text" placeholder="Введите логин" class="form-control " name="login" id="validationCustomUsername"
                           aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                        Введите имя пользователя
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="validationCustompass" class="col-sm-3 control-label">Пароль</label>
                <div class="col-sm-10 mx-auto">
                    <input type="password" placeholder="Введите пароль" class="form-control" name="pass" id="validationCustompass"
                           aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                        Введите пароль
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-primary mb-2" value="Войти">
    </form>
    <form action="registration.php">
        <input type="submit" class="btn btn-primary mb-2" value="Зарегистрироваться">
    </form>
</div>
</body>
</html>

